package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	logger  *log.Logger
	history map[string]Rate
)

// Rate represents financing rate for an instrument pair
type Rate struct {
	Currency    string  `json:"currency"`
	Days        int     `json:"days"`
	Instrument  string  `json:"instrument"`
	LongCharge  float64 `json:"longCharge,string"`
	LongRate    float64 `json:"longRate,string"`
	ShortCharge float64 `json:"shortCharge,string"`
	ShortRate   float64 `json:"shortRate,string"`
	Units       int     `json:"units"`
}

// Financing represents response from oanda api
type Financing struct {
	Rates     []Rate    `json:"financingRates"`
	Timestamp time.Time `json:"timestamp"`
}

type slackMsg struct {
	Text string `json:"text"`
}

func main() {

	webhook := getEnv("SLACK", "")

	watchPair := []string{"USD/JPY", "EUR/USD"}

	history := make(map[string]Rate)

	logger = log.New()
	logger.SetOutput(os.Stdout)
	logger.SetLevel(log.DebugLevel)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)

	ticker := time.NewTicker(2 * time.Minute)
	done := make(chan bool)

	go func(watchPair []string, webhook string, history map[string]Rate) {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				run(t, watchPair, webhook, history)
			}
		}
	}(watchPair, webhook, history)

	go func() {
		sig := <-sigs
		logger.Infof("%v signal received\n", sig)
		ticker.Stop()
		close(done)
		logger.Infoln("Ticker stopped")
	}()

	logger.Infoln("Application started")
	<-done
	logger.Infoln("Exiting...")
}

func run(t time.Time, watchPair []string, webhook string, history map[string]Rate) {
	logger.Printf("Task started at %v\n", t)
	financing, err := fetchFinancing()

	if err != nil {
		logger.Errorln(err)
		return
	}

	for _, pair := range watchPair {
		newRate := financing.getRatesMap()[pair]
		oldRate, oldRateExist := history[pair]

		if !oldRateExist {
			logger.Infof("No history of pair \"%v\", send rate to slack anyway", pair)
			notifyRateChange(newRate, webhook)
			history[pair] = newRate
			continue
		}

		if newRate != oldRate {
			logger.Infof("Changes found for \"%v\", send rate to slack", pair)
			notifyRateChange(newRate, webhook)
			history[pair] = newRate
			continue
		}

		logger.Infof("No rate change found for \"%v\"", pair)
	}
}

func (f Financing) getRatesMap() map[string]Rate {
	m := make(map[string]Rate)

	for _, rate := range f.Rates {
		m[rate.Instrument] = rate
	}
	return m
}

func fetchFinancing() (*Financing, error) {
	logger.Printf("Fetching rate %v", rateURL().String())

	resp, err := http.Get(rateURL().String())

	time.Now().Format(time.RFC3339)

	if err != nil {
		logger.Println("Unable to retrieve financing rate")
		return nil, err
	}

	defer resp.Body.Close()

	logger.Infoln("Retrieved financing rate")

	jsonRaw, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		logger.Errorln("Failed to read response body")
		logger.Errorln(err)
		return nil, err
	}

	var financing Financing

	if err := json.Unmarshal(jsonRaw, &financing); err != nil {
		logger.Errorln("Failed to read response as json")
		logger.Errorln(err)
		return nil, err
	}
	return &financing, nil
}

func notifyRateChange(r Rate, webhookURL string) {
	err := postSlack(webhookURL, r.toSlackMessage())

	if err != nil {
		logger.Errorln(err)
	}
}

func (r Rate) toSlackMessage() string {
	return fmt.Sprintf("[%v] L:%v/S:%v @%v units", r.Instrument, r.LongCharge, r.ShortCharge, r.Units)
}

func postSlack(webhookURL string, message string) error {
	if webhookURL == "" {
		logger.Errorln("slack webhook is empty! not attempt to post message to slack")
		return errors.New("slack webhook is empty")
	}

	msg, _ := json.Marshal(slackMsg{Text: message})

	req, err := http.NewRequest(http.MethodPost, webhookURL, bytes.NewBuffer(msg))

	if err != nil {
		logger.Errorln(err)
		return err
	}

	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)

	if err != nil {
		logger.Errorln(err)
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		logger.Errorf("Expected status 200 from Slack but got %v\n", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	if "ok" != string(body) {
		logger.Errorf("Expected response ok from Slack but got %v\n", string(body))
	}

	return nil
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func rateURL() *url.URL {
	base, _ := url.Parse("https://fxserver01-ng.oanda.com/marketData/v1/financingRates")

	// Query params
	params := url.Values{}
	params.Add("divisionID", "3")
	params.Add("tradingGroupID", "2")
	params.Add("time", time.Now().UTC().Format(time.RFC3339))

	base.RawQuery = params.Encode()

	return base
}
