FROM golang:1.13-alpine AS builder

ADD . /srv
WORKDIR /srv
RUN apk add --update upx && \
    go mod download && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -a -o /app . && \
    upx /app

FROM alpine:3.10

COPY --from=builder /app .
RUN chmod +x ./app

ENTRYPOINT ["./app"]

USER nobody
