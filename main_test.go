package main

import (
	"testing"
)

func TestItGeneratesCorrectSlackMessage(t *testing.T) {
	var tests = []struct {
		r      Rate
		expect string
	}{
		{
			r: Rate{
				Currency:    "USD",
				Days:        01,
				Instrument:  "USD/JPY",
				LongCharge:  13.15,
				LongRate:    0.048,
				ShortCharge: -23.45,
				ShortRate:   -0.0856,
				Units:       100000,
			},
			expect: "[USD/JPY] L:13.15/S:-23.45 @100000 units",
		},
	}

	for _, test := range tests {
		actual := test.r.toSlackMessage()
		if actual != test.expect {
			t.Errorf("Expect \"%v\" but got \"%v\"", test.expect, actual)
		}
	}
}
