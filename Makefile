BINARY:=app

default: lint test build exec

.phony: run
run:
	go build -o app && ./app

.phony: lint
lint:
	go get -t -v ./...
	go fmt .
	golint -set_exit_status .
	go vet $(go list ./... | grep -v /vendor/)

.phony: build
build:
	go build -o $(BINARY)

.phony: exec
exec:
	./app

.phony: test
test:
	go test -v -race -cover ./...

.phony: docker
docker:
	docker build -t oanda-financing:local .
